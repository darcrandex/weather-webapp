import { get } from '../tools/axios'

function apiWeatherNow() {
  const url = '/now'
  return get(url)
}
function apiWeatherHourly() {
  const url = '/hourly'
  return get(url)
}
function apiWeatherDaily() {
  const url = '/daily'
  return get(url)
}
function apiLifeStyle() {
  const url = '/life-style'
  return get(url)
}
function apiGetDoSomething() {
  const url = '/do-something'
  return get(url)
}
function apiGetLuck() {
  const url = '/luck'
  return get(url)
}
function apiGetLots() {
  const url = '/draw-lots'
  return get(url)
}
function apiGetHistoryMood() {
  const url = '/history-mood'
  return get(url)
}
export {
  apiWeatherNow,
  apiWeatherHourly,
  apiWeatherDaily,
  apiLifeStyle,
  apiGetDoSomething,
  apiGetLuck,
  apiGetLots,
  apiGetHistoryMood
}
