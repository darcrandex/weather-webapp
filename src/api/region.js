/**
 * @desc 高德地图,查询行政区域api
 */

import { get } from '../tools/axios'

const devKey = 'fdca9b0a1091295f854902b3e4d1bfa6'

function apiSearchRegion(keywords) {
  const url = 'http://restapi.amap.com/v3/config/district'
  return get(url, { key: devKey, keywords, subdistrict: 0 })
}

function apiGetWeather({ adcode, type = 'base' }) {
  const url = 'http://restapi.amap.com/v3/weather/weatherInfo'
  return get(url, { key: devKey, city: adcode, extensions: type })
}

export { apiSearchRegion, apiGetWeather }
