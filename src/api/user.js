import { post } from '../tools/axios'

function apiSignUp({ email, password }) {
  const url = '/sign-up'
  return post(url, { email, password })
}

function apiLogin(params) {
  const url = '/login'
  return post(url, params)
}

function apiGetUserInfo({ token, username }) {
  const url = '/user'
  return post(url, { username }, { headers: { Authorization: token } })
}

function apiUpdateUserInfo({ token, nickname, avatar, email, conscode }) {
  const url = '/update-user-info'
  return post(url, { nickname, avatar, email, conscode }, { headers: { Authorization: token } })
}

export { apiSignUp, apiLogin, apiGetUserInfo, apiUpdateUserInfo }
