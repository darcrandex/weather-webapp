import {
  apiWeatherNow,
  apiWeatherHourly,
  apiWeatherDaily,
  apiLifeStyle,
  apiGetDoSomething,
  apiGetLuck,
  apiGetLots
} from '@/api/weather'
import { setState } from './index'

const state = {
  netWorkError: false,
  weatherNow: {},
  weatherNowLoading: false,
  weatherHourly: {},
  weatherHourlyLoading: false,
  weatherDaily: {},
  weatherDailyLoading: false,
  lifeStyle: {},
  lifeStyleLoading: false,
  calendar: {},
  calendarLoading: false,
  luck: [],
  luckLoading: false,
  lots: {},
  isGetLots: false,
  lotsLoading: false
}
const mutations = {
  setState
}
const actions = {
  getWeatherNow({ commit }) {
    commit('setState', { weatherNowLoading: true })
    apiWeatherNow().then(result => {
      if (result.status === 'ok') {
        commit('setState', {
          weatherNow: result.data,
          weatherNowLoading: false
        })
      }
    })
  },

  getWeatherHourly({ commit }) {
    commit('setState', { weatherHourlyLoading: true })
    apiWeatherHourly().then(result => {
      if (result.status === 'ok') {
        commit('setState', {
          weatherHourly: result.data,
          weatherHourlyLoading: false
        })
      }
    })
  },

  getWeatherDaily({ commit }) {
    commit('setState', { weatherDailyLoading: true })
    apiWeatherDaily().then(result => {
      if (result.status === 'ok') {
        commit('setState', {
          weatherDaily: result.data,
          weatherDailyLoading: false
        })
      }
    })
  },

  async getLifeStyle({ commit }) {
    commit('setState', { lifeStyleLoading: true })
    await apiLifeStyle().then(result => {
      if (result.status === 'ok') {
        commit('setState', {
          lifeStyle: result.data,
          lifeStyleLoading: false
        })
      }
    })
  },

  getCalendar({ commit }) {
    commit('setState', { calendarLoading: true })
    apiGetDoSomething().then(result => {
      if (result.status === 'ok') {
        commit('setState', { calendar: result.data, calendarLoading: false })
      }
    })
  },

  async getLuck({ commit }) {
    commit('setState', { luckLoading: true })
    await apiGetLuck().then(result => {
      if (result.status === 'ok') {
        commit('setState', { luck: result.data, luckLoading: false })
      }
    })
  },

  getLots({ commit }) {
    commit('setState', { lotsLoading: true })
    apiGetLots().then(result => {
      if (result.status === 'ok') {
        commit('setState', { lots: result.data, lotsLoading: false, isGetLots: true })
      }
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
