const state = {
  cards: [
    { name: '老年历', value: 'lnl', checked: true },
    { name: '抽签', value: 'cq', checked: true },
    { name: '我的心情', value: 'wdxq', checked: true },
    { name: '星座运势', value: 'xzys', checked: true }
  ]
}
const mutations = {
  check(state, value) {
    state.cards = state.cards.map(v => ({
      ...v,
      checked: v.value === value ? !v.checked : v.checked
    }))
  }
}

export default { namespaced: true, state, mutations }
