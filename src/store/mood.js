import { apiGetHistoryMood } from '@/api/weather'
import { setState } from './index'

const state = {
  moodCode: 0,
  moodArticle: '',
  isEdited: false,
  moodHistory: [],
  moodHistoryLoading: false
}
const mutations = {
  setState,
  updateMood(state, { code, article }) {
    state.moodCode = code
    state.moodArticle = article
    state.isEdited = true
  }
}
const actions = {
  getHistoryMood({ commit }) {
    commit('setState', { moodHistoryLoading: true })
    apiGetHistoryMood().then(result => {
      if (result.status === 'ok') {
        commit('setState', { moodHistoryLoading: false, moodHistory: result.data })
      }
    })
  }
}
export default {
  namespaced: true,
  state,
  mutations,
  actions
}
