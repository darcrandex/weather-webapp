import { setState } from './index'

const duration = 3000
const state = {
  toasts: []
}
const mutations = {
  setState
}
const actions = {
  addToast({ state, commit }, { type = 'info', title, content }) {
    const id = new Date().getTime()
    commit('setState', { toasts: [{ id, type, title, content }, ...state.toasts] })
    const timer = setTimeout(() => {
      commit('setState', { toasts: state.toasts.filter(v => v.id !== id) })
      clearTimeout(timer)
    }, duration)
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
