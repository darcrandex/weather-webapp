import Vue from 'vue'
import Vuex from 'vuex'
import Home from './home'
import Mood from './mood'
import User from './user'
import Cards from './cards'
import Toast from './toast'
import Region from './region'

Vue.use(Vuex)

/**
 * @name: global-mutation
 * @param {Object} state module state
 * @param {Object} options state that need to update
 */

export function setState(state, options) {
  Object.keys(options).forEach(key => {
    if (Object.prototype.hasOwnProperty.call(state, key)) {
      state[key] = options[key]
    }
  })
}

export default new Vuex.Store({
  modules: { Home, Mood, User, Cards, Toast, Region }
})
