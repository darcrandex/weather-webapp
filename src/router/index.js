import Vue from 'vue'
import Router from 'vue-router'
import MainPage from '@/view/MainPage'

const Home = () => import('@/view/Home')
const Helpers = () => import('@/view/Helpers')
const Find = () => import('@/view/Find')
const MoodEdit = () => import('@/view/MoodEdit')
const MoodHistory = () => import('@/view/MoodHistory')
const LoginPanel = () => import('@/view/Find/LoginPanel')
const CardsAdmin = () => import('@/view/CardsAdmin')
const CalendarDetail = () => import('@/view/CalendarDetail')
const DrawLotsDetail = () => import('@/view/DrawLotsDetail')
const ConstellationDetail = () => import('@/view/ConstellationDetail')
const SignUp = () => import('@/view/Find/SignUp')
const ConstellationChoose = () => import('@/view/ConstellationChoose')
const UserInfoPanel = () => import('@/view/UserInfoPanel')
const RegionSelect = () => import('@/view/Region/RegionSelect')
const RegionAdd = () => import('@/view/Region/RegionAdd')

Vue.use(Router)

const router = new Router({
  linkActiveClass: 'linked',
  linkExactActiveClass: 'linking',
  routes: [
    {
      path: '/',
      redirect: '/home',
      name: 'main-page',
      component: MainPage,
      children: [
        {
          path: '/home',
          name: 'home',
          component: Home
        },
        {
          path: '/helpers',
          name: 'helpers',
          component: Helpers
        },
        {
          path: '/find',
          name: 'find',
          component: Find
        }
      ]
    },
    {
      path: '/mood-edit',
      name: 'mood-edit',
      component: MoodEdit
    },
    {
      path: '/mood-history',
      name: 'mood-history',
      component: MoodHistory
    },
    {
      path: '/find/login-panel',
      name: 'login-panel',
      component: LoginPanel
    },
    {
      path: '/helpers/cards-admin',
      name: 'cards-admin',
      component: CardsAdmin
    },
    {
      path: '/helpers/calendar-detail',
      name: 'calendar-detail',
      component: CalendarDetail
    },
    {
      path: '/helpers/drawlots-detail',
      name: 'drawlots-detail',
      component: DrawLotsDetail
    },
    {
      path: '/helpers/constellation-detail',
      name: 'constellation-detail',
      component: ConstellationDetail
    },
    {
      path: '/find/sign-up',
      name: 'sign-up',
      component: SignUp
    },
    {
      path: '/find/cons-choose',
      name: 'cons-choose',
      component: ConstellationChoose
    },
    {
      path: '/find/user-info',
      name: 'user-info',
      component: UserInfoPanel
    },
    {
      path: '/region-select',
      name: 'region-select',
      component: RegionSelect
    },
    {
      path: '/region-select/region-add',
      name: 'region-add',
      component: RegionAdd
    }
  ]
})

router.beforeEach((to, from, next) => {
  window.document.title = to.meta.title || '米丫天气'
  next()
})

export default router
