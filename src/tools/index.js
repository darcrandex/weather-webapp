// weather icons
import dafeng from '@/assets/icons/w-dafeng.png'
import longjuanfeng from '@/assets/icons/w-longjuanfeng.png'
import richu from '@/assets/icons/w-richu.png'
import riluo from '@/assets/icons/w-riluo.png'
import shachen from '@/assets/icons/w-shachen.png'
import baoxue from '@/assets/icons/w-baoxue.png'
import baoyu from '@/assets/icons/w-baoyu.png'
import bingbao from '@/assets/icons/w-bingbao.png'
import daxue from '@/assets/icons/w-daxue.png'
import dayu from '@/assets/icons/w-dayu.png'
import dongyu from '@/assets/icons/w-dongyu.png'
import duoyunzhuanyu from '@/assets/icons/w-duoyunzhuanyu.png'
import leizhenyu from '@/assets/icons/w-leizhenyu.png'
import qingtian from '@/assets/icons/w-qingtian.png'
import qingzhuanduoyun from '@/assets/icons/w-qingzhuanduoyun.png'
import shandian from '@/assets/icons/w-shandian.png'
import wu from '@/assets/icons/w-wu.png'
import wumai from '@/assets/icons/w-wumai.png'
import xiaoxue from '@/assets/icons/w-xiaoxue.png'
import xiaoyu from '@/assets/icons/w-xiaoyu.png'
import yangchen from '@/assets/icons/w-yangchen.png'
import yintian from '@/assets/icons/w-yintian.png'
import yujiaxue from '@/assets/icons/w-yujiaxue.png'
import zhongxue from '@/assets/icons/w-zhongxue.png'
import zhongyu from '@/assets/icons/w-zhongyu.png'

import aq3 from '@/assets/icons/aq-4.png'
import aq4 from '@/assets/icons/aq-5.png'

// mood icons
import mood1 from '@/assets/images/mood-1.png'
import mood2 from '@/assets/images/mood-2.png'
import mood3 from '@/assets/images/mood-3.png'
import mood4 from '@/assets/images/mood-4.png'
import mood5 from '@/assets/images/mood-5.png'
import mood1a from '@/assets/images/mood-1-a.png'
import mood2a from '@/assets/images/mood-2-a.png'
import mood3a from '@/assets/images/mood-3-a.png'
import mood4a from '@/assets/images/mood-4-a.png'
import mood5a from '@/assets/images/mood-5-a.png'
import mood1l from '@/assets/images/mood-1-l.png'
import mood2l from '@/assets/images/mood-2-l.png'
import mood3l from '@/assets/images/mood-3-l.png'
import mood4l from '@/assets/images/mood-4-l.png'
import mood5l from '@/assets/images/mood-5-l.png'

// Constellation icons
import cons1 from '@/assets/icons/cons1.png'
import cons2 from '@/assets/icons/cons2.png'
import cons3 from '@/assets/icons/cons3.png'
import cons4 from '@/assets/icons/cons4.png'
import cons5 from '@/assets/icons/cons5.png'
import cons6 from '@/assets/icons/cons6.png'
import cons7 from '@/assets/icons/cons7.png'
import cons8 from '@/assets/icons/cons8.png'
import cons9 from '@/assets/icons/cons9.png'
import cons10 from '@/assets/icons/cons10.png'
import cons11 from '@/assets/icons/cons11.png'
import cons12 from '@/assets/icons/cons12.png'

// 将高德地图提供的天气现象文本转化为对应的code
function textToCode(text) {
  const textMap = [
    { text: '晴', code: 13 },
    { text: '晴天', code: 13 },
    { text: '多云', code: 14 },
    { text: '阴', code: 20 },
    { text: '阵雨', code: 12 },
    { text: '雷阵雨', code: 12 },
    { text: '雷阵雨并伴有冰雹', code: 12 },
    { text: '雨夹雪', code: 21 },
    { text: '小雨', code: 18 },
    { text: '中雨', code: 23 },
    { text: '大雨', code: 23 },
    { text: '暴雨', code: 6 },
    { text: '大暴雨', code: 6 },
    { text: '特大暴雨', code: 6 },
    { text: '阵雪', code: 17 },
    { text: '小雪', code: 17 },
    { text: '中雪', code: 5 },
    { text: '大雪', code: 5 },
    { text: '暴雪', code: 5 },
    { text: '雾', code: 16 },
    { text: '冻雨', code: 10 },
    { text: '沙尘暴', code: 19 },
    { text: '小雨-中雨', code: 18 },
    { text: '中雨-大雨', code: 23 },
    { text: '大雨-暴雨', code: 9 },
    { text: '暴雨-大暴雨', code: 13 },
    { text: '大暴雨-特大暴雨', code: 13 },
    { text: '小雪-中雪', code: 18 },
    { text: '中雪-大雪', code: 5 },
    { text: '大雪-暴雪', code: 5 },
    { text: '浮尘', code: 19 },
    { text: '扬沙', code: 19 },
    { text: '强沙尘暴', code: 19 },
    { text: '飑', code: 19 },
    { text: '龙卷风', code: 1 },
    { text: '弱高吹雪', code: 8 },
    { text: '轻雾', code: 15 },
    { text: '霾', code: 16 }
  ]
  return textMap.find(v => v.text === text).code
}

// 通过 code 来返回天气表现对象
function getWeatherObj(weatherCode) {
  const weatherMap = [
    { text: '大风', icon: dafeng },
    { text: '龙卷风', icon: longjuanfeng },
    { text: '日出', icon: richu },
    { text: '日落', icon: riluo },
    { text: '沙尘', icon: shachen },
    { text: '暴雪', icon: baoxue },
    { text: '暴雨', icon: baoyu },
    { text: '冰雹', icon: bingbao },
    { text: '大雪', icon: daxue },
    { text: '大雨', icon: dayu },
    { text: '冻雨', icon: dongyu },
    { text: '多云转雨', icon: duoyunzhuanyu },
    { text: '雷阵雨', icon: leizhenyu },
    { text: '晴天', icon: qingtian },
    { text: '晴转多云', icon: qingzhuanduoyun },
    { text: '闪电', icon: shandian },
    { text: '雾', icon: wu },
    { text: '雾霾', icon: wumai },
    { text: '小雪', icon: xiaoxue },
    { text: '小雨', icon: xiaoyu },
    { text: '扬尘', icon: yangchen },
    { text: '阴天', icon: yintian },
    { text: '雨夹雪', icon: yujiaxue },
    { text: '中雪', icon: zhongxue },
    { text: '中雨', icon: zhongyu }
  ]
  return weatherMap[weatherCode]
}
function getAirQuality(level) {
  const quality = [
    { text: '极差', icon: aq3 },
    { text: '差', icon: aq3 },
    { text: '一般', icon: aq3 },
    { text: '良', icon: aq3 },
    { text: '优', icon: aq4 }
  ]
  return quality[level]
}
function getYearGZ() {
  const time = new Date()
  const y = time.getFullYear()
  const g = ['庚', '辛', '壬', '癸', '甲', '乙', '丙', '丁', '戊', '己']
  const z = ['申', '酉', '戌', '亥', '子', '丑', '寅', '卯', '辰', '巳', '午', '未']
  return `${g[y % 10]}${z[y % 12]}`
}
function getMonthGZ() {
  const time = new Date()
  const m = time.getMonth()
  const map = [
    [
      '丁丑',
      '丙寅',
      '丁卯',
      '戊辰',
      '己巳',
      '庚午',
      '辛未',
      '壬申',
      '癸酉',
      '甲戌',
      '乙亥',
      '丙子'
    ],
    [
      '己丑',
      '戊寅',
      '己卯',
      '庚辰',
      '辛巳',
      '壬午',
      '癸未',
      '甲申',
      '乙酉',
      '丙戌',
      '丁亥',
      '戊子'
    ],
    [
      '辛丑',
      '庚寅',
      '辛卯',
      '壬辰',
      '癸巳',
      '甲午',
      '乙未',
      '丙申',
      '丁酉',
      '戊戌',
      '己亥',
      '庚子'
    ],
    [
      '癸丑',
      '壬寅',
      '癸卯',
      '甲辰',
      '乙巳',
      '丙午',
      '丁未',
      '戊申',
      '己酉',
      '庚戌',
      '辛亥',
      '壬子'
    ],
    ['乙丑', '甲寅', '乙卯', '丙辰', '丁巳', '戊午', '己未', '庚申', '辛酉', '壬戌', '癸亥', '甲子']
  ]
  let index = Math.floor('甲己乙庚丙辛丁壬戊癸'.indexOf(getYearGZ()[0]) / 2)
  return map[index][m]
}
function getDateGZ() {
  const map = [
    '',
    '甲子',
    '乙丑',
    '丙寅',
    '丁卯',
    '戊辰',
    '己巳',
    '庚午',
    '辛未',
    '壬申',
    '癸酉',
    '甲戌',
    '乙亥',
    '丙子',
    '丁丑',
    '戊寅',
    '己卯',
    '庚辰',
    '辛巳',
    '壬午',
    '癸未',
    '甲申',
    '乙酉',
    '丙戌',
    '丁亥',
    '戊子',
    '己丑',
    '庚寅',
    '辛卯',
    '壬辰',
    '癸巳',
    '甲午',
    '乙未',
    '丙申',
    '丁酉',
    '戊戌',
    '己亥',
    '庚子',
    '辛丑',
    '壬寅',
    '癸卯',
    '甲辰',
    '乙巳',
    '丙午',
    '丁未',
    '戊申',
    '己酉',
    '庚戌',
    '辛亥',
    '壬子',
    '癸丑',
    '甲寅',
    '乙卯',
    '丙辰',
    '丁巳',
    '戊午',
    '己未',
    '庚申',
    '辛酉',
    '壬戌',
    '癸亥'
  ]
  const monthBase = [0, 31, -1, 30, 0, 31, 1, 32, 3, 33, 4, 34]
  // 17+
  const centuryBase = [0, 47, 31, 15, 0, 44, 28, 12, 57, 41]
  const time = new Date()
  const y = time.getFullYear()
  const m = time.getMonth()
  const d = time.getDate()
  const s = y % 100 - 1
  const u = s % 4
  const mBase = monthBase[m]
  const x = centuryBase[Math.ceil(y / 100) - 17]
  const index = 6 * Math.floor(s / 4) + 5 * (3 * Math.floor(s / 4) + u) + mBase + d + x
  return map[index % 60]
}
function getCZ() {
  const map = [
    '亥猪',
    '子鼠',
    '丑牛',
    '寅虎',
    '卯兔',
    '辰龙',
    '巳蛇',
    '午马',
    '未羊',
    '申猴',
    '酉鸡',
    '戌狗'
  ]
  const y = new Date().getFullYear()
  return map[(y - 3) % 12]
}
function getMoodIcon(code) {
  const icons = [mood1, mood2, mood3, mood4, mood5]
  const iconsActive = [mood1a, mood2a, mood3a, mood4a, mood5a]
  const iconsLong = [mood1l, mood2l, mood3l, mood4l, mood5l]
  return { icon: icons[code - 1], iconActive: iconsActive[code - 1], iconLong: iconsLong[code - 1] }
}
function getConsObj(code) {
  const map = [
    { text: '白羊座', icon: cons1 },
    { text: '金牛座', icon: cons2 },
    { text: '双子座', icon: cons3 },
    { text: '巨蟹座', icon: cons4 },
    { text: '狮子座', icon: cons5 },
    { text: '处女座', icon: cons6 },
    { text: '天秤座', icon: cons7 },
    { text: '天蝎座', icon: cons8 },
    { text: '射手座', icon: cons9 },
    { text: '摩羯座', icon: cons10 },
    { text: '水瓶座', icon: cons11 },
    { text: '双鱼座', icon: cons12 }
  ]
  return map[code]
}

export {
  textToCode,
  getWeatherObj,
  getAirQuality,
  getYearGZ,
  getMonthGZ,
  getDateGZ,
  getCZ,
  getMoodIcon,
  getConsObj
}
