> 模仿'米丫天气'

## 1. 改用'stylus'

## 2. 使用'stylus 全局变量'

在`build/utils.js的generateLoaders`中修改.
定义全局变量的文件配置

```
const stylusOptions = {
  import: [path.join(__dirname, '../src/assets/style/theme.styl')],
  paths: [path.join(__dirname, '../src/assets/style/'), path.join(__dirname, '../')]
}
```

其中的`theme.styl`定义全局变量的文件

修改`stylus`的配置

```
stylus: generateLoaders('stylus', stylusOptions),
styl: generateLoaders('stylus', stylusOptions)
```

使用时,直接在样式用引用定义的变量即可.
